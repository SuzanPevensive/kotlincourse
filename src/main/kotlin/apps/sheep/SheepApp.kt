package apps.sheep

import apps.base.BaseApp

class SheepApp : BaseApp() {
    override fun main(vararg args: String) {
        console.log("apps.base.App start")
        console.log("Ile owiec ma ci sie przyśnić?")
//        val sheepNumber = readLine()?.toIntOrNull() ?: 0
//        val sheepNumber = try {
//            readLine()?.toInt() ?: 0
//        } catch (e: Exception) {
//            0
//        }
        var sheepNumber = readLine()?.toIntOrNull()
        while (sheepNumber == null) {
            console.log("Podaj prawidłową wartosć liczbową:")
            sheepNumber = readLine()?.toIntOrNull()
        }
        showSheepHerdList(sheepNumber)
    }

    fun showSheepHerdList(sheepNumber: Int) {
        val sheepHerdList = mutableListOf<String>()
        for (i: Int in 0 until sheepNumber) {
            sheepHerdList.add("owca")
        }
        console.log(sheepHerdList.joinToString(", "))
    }

    fun showSheepHerdString(sheepNumber: Int) {
        var sheepHerdString = ""
        for (i: Int in 0 until sheepNumber) {
            sheepHerdString += "owca, "
        }
        console.log(sheepHerdString)
    }
}
