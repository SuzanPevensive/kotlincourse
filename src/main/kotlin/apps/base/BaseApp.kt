package apps.base

import managers.ConsoleManager

abstract class BaseApp {
    protected val console = ConsoleManager()
    abstract fun main(vararg args: String)
}
