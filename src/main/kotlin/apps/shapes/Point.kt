package apps.shapes

data class Point(private val char: Char) {
    fun draw() { print(char) }
}
