package apps.shapes

data class Line(private val points: List<Point>) {
    fun draw() {
        points.forEach { point ->
            point.draw()
        }
    }
}
