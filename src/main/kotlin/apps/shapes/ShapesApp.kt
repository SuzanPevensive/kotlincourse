package apps.shapes

import apps.base.BaseApp

class ShapesApp : BaseApp() {

    override fun main(vararg args: String) {
        console.log("Podaj szerokość kwadratu:")
        val width = readLine()?.toIntOrNull() ?: 0
        console.log("Podaj wysokość kwadratu:")
        val height = readLine()?.toIntOrNull() ?: 0
        val shape = createSquare(width, height)
        shape.draw()
    }

    fun createSquare(width: Int, height: Int): Shape {
        val lines = mutableListOf<Line>()
        for (y in 0 until height) {
            val points = mutableListOf<Point>()
            for (x in 0 until width) {
                points.add(Point('.'))
            }
            lines.add(Line(points))
        }
        return Shape(lines)
    }
}
