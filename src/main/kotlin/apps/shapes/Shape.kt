package apps.shapes

data class Shape(private val lines: List<Line>) {
    fun draw() {
        lines.forEach { line ->
            line.draw()
            println()
        }
    }
}
