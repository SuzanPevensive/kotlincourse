import apps.base.BaseApp
import apps.sheep.SheepApp

val app: BaseApp = SheepApp()

fun main(args: Array<String>) {
    app.main(*args)
}
