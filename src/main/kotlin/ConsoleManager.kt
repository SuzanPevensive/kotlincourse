class ConsoleManager {
    fun log(text: String, hidePrefix: Boolean = true) {
        if (hidePrefix) {
            println(text)
        } else {
            println("Console log:\n$text")
        }
    }
}
